package com.cice.corrutinas

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import kotlin.math.log

class GlobalViewModel : ViewModel() {

    companion object {
        fun create(activity: GlobalActivity): GlobalViewModel {
            Log.d("CICE","Create View Model")
            return ViewModelProviders.of(activity).get(GlobalViewModel::class.java)
        }
    }

    var joke : MutableLiveData<String> = MutableLiveData()
}