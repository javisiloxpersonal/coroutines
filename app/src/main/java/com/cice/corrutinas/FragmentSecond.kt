package com.cice.corrutinas

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_second.*

class FragmentSecond : Fragment(), LifecycleOwner {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_second, container, false)
        Log.d("CICE", "Fragment Second")
        (activity as GlobalActivity).viewModel.joke.observe(this, Observer<String> { t ->
            Log.d("CICE", "OBSERVER")
            textSecondJoke.text = t
        })
        return view
    }
}