package com.cice.corrutinas

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner

class FragmentFirst : Fragment(), LifecycleOwner {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.activity_main, container, false)
        Log.d("CICE", "Fragment First")
        activity as GlobalActivity
        (activity as GlobalActivity).viewModel = GlobalViewModel.create(activity as GlobalActivity)
        return view
    }
}