package com.cice.corrutinas

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.uiThread
import java.net.URL
import kotlin.coroutines.CoroutineContext


class MainActivity : GlobalActivity() {

    companion object {
        const val RANDOM_URL = "https://api.chucknorris.io/jokes/random"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(FragmentFirst())
        getRadomJoke()
    }

    private fun replaceFragment(fragment: Fragment) {
        Log.d("CICE 4", "replace fragment")
        supportFragmentManager.beginTransaction().replace(R.id.frameContainer, fragment).commit()
    }


    private fun getRadomJoke() {
        getJokeUsingCorutines()
    }


    private fun getJokeUsingCorutines() {
        launch {
            withContext(Dispatchers.IO) { shitTask() }
            var joke = withContext(Dispatchers.IO) { URL(RANDOM_URL).readText() }
            Log.d("CICE 4", joke)
            viewModel.joke.value = joke
        }
        replaceFragment(FragmentSecond())
    }


    private fun getJokeUsingAnko() {
        doAsync {

            var url = "https://api.chucknorris.io/jokes/random"
            var joke = URL(url).readText()

            //PROBLEMA!!!
            //ESTA SEGUNDA PETICION ES INVIABLE YA QUE CUANDO SE EJECUTA ESTA LINEA
            //AUN NO TENEMOS EL RESULTADO DE LA PRIMERA
            Log.d("CICE", "JOKE 2")
            var joke2 = URL(url).readText()



            uiThread {
                Log.d("CICE1", joke)
                Log.d("CICE2", joke2)


                //Log.d("CICE", joke2)

            }
        }
    }

    suspend fun shitTask() {
        Log.d("CICE", "In DELAY")

        delay(4000L)
        Log.d("CICE", "Out DELAY")
    }


    private fun getJokeWithoutAnko() {
        object : AsyncTask<Void, Void, String>() {

            var url = ""

            override fun onPreExecute() {
                super.onPreExecute()
                url = "https://api.chucknorris.io/jokes/random"
            }

            override fun doInBackground(vararg params: Void?): String? {
                //shitTask()
                var joke = URL(url).readText()
                Log.d("CICE1", joke)
                return joke
            }

            override fun onPostExecute(result: String) {
                Log.d("CICE", result)
                super.onPostExecute(result)
            }
        }.execute()
    }

}
